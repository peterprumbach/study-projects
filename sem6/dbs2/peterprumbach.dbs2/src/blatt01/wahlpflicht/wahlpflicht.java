package blatt01.wahlpflicht;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class wahlpflicht {

    private static SessionFactory sessionFactory;

    public static void main(String[] args) {
        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
				.configure("/blatt01/wahlpflicht/hibernate.cfg.xml").build();
		Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
		sessionFactory = metaData.getSessionFactoryBuilder().build();
        
        init();
    }

    public static void init() {
        Student studenten[] = {
            new Student (105, "Schmidt"),
            new Student (102, "Meier"),
            new Student (103, "Schulze")
        };

        Modul module[] = {
            new Modul(5, "Cybersecurity", "IKB", "TIB")
            .einschreiben (studenten[0])
            .einschreiben (studenten[1]),
            new Modul(6, "Echtzeitbildanalyse", "IKB")
            .einschreiben (studenten[2]),
            new Modul(6, "Datenbanksysteme 2", "TIB")
            .einschreiben (studenten[1])
            .einschreiben (studenten[2])
            .einschreiben (studenten[0])
            };

        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();
        for (Student s: studenten) session.save(s);
        for (Modul m: module) session.save(m);
        tx.commit();
    }
}
