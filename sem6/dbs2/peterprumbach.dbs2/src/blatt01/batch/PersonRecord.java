package blatt01.batch;

public class PersonRecord {
    long id;
    String code;

    public PersonRecord(long id, String code) {
        this.id = id;
        this.code = code;
    }
}