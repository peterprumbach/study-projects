package blatt01.batch;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class batch {
    
    static Connection con;

    final static String SQLdriver = "org.postgresql.Driver";
	final static String JDBC_URL = "jdbc:postgresql://localhost:5432/postgres"; 
	final static String user = "postgres";
	final static String password = "dbs";

    public static void main(String[] args) throws Exception {
		Class.forName(SQLdriver); 
		con = DriverManager.getConnection(JDBC_URL, user, password);
		
        //init();

        //Aufgabe1a();
        Aufgabe1b();
        //Aufgabe1c();

        //Aufgabe2();

        con.close();
    }

    public static void init() throws Exception {
        PreparedStatement ps = con.prepareStatement("CREATE SCHEMA person");
        ps.executeUpdate();
        ps.close();

        ps = con.prepareStatement("CREATE TABLE person.person ( " +
            "id SERIAL PRIMARY KEY, " + 
            "code VARCHAR(16))");
        ps.executeUpdate();
        ps.close();
    }

    public static void Aufgabe1a() throws Exception {
        System.out.printf("Anzahl\tAutoCommit\tDauer (s)\n");

        System.out.printf("%d\t%s\t%d\n", 10000, "false", insertData(10000, 0, false));
        con.commit();
        System.out.printf("%d\t%s\t%d\n", 10000, "true", insertData(10000, 0, true));
    }

    public static void Aufgabe1b() throws Exception {
        System.out.printf("Anzahl\tAutoCommit\tBatchSize\tDauer (s)\n");

        System.out.printf("%d\t%s\t%s\t%d\n", 10000, "true", 1, insertData(10000, 1, true));
        System.out.printf("%d\t%s\t%s\t%d\n", 10000, "false", 1,insertData(10000, 1, false));
        con.commit();
        System.out.printf("%d\t%s\t%s\t%d\n", 10000, "true", 10, insertData(10000, 10, true));
        System.out.printf("%d\t%s\t%s\t%d\n", 10000, "false", 10, insertData(10000, 10, false));
        con.commit();
        System.out.printf("%d\t%s\t%s\t%d\n", 10000, "true", 100, insertData(10000, 100, true));
        System.out.printf("%d\t%s\t%s\t%d\n", 10000, "false", 100, insertData(10000, 100, false));
        con.commit();
    }

    public static void Aufgabe1c() throws Exception {
        System.out.printf("Anzahl\tAutoCommit\tBatchSize\tDauer (s)\n");

        System.out.printf("%d\t%s\t%s\t%d\n", 1000000, "true", 10, insertData(1000000, 10, false));
        System.out.printf("%d\t%s\t%s\t%d\n", 1000000, "false", 100, insertData(1000000, 100, false));
        System.out.printf("%d\t%s\t%s\t%d\n", 1000000, "true", 1000, insertData(1000000, 1000, false));
        System.out.printf("%d\t%s\t%s\t%d\n", 1000000, "false", 10000, insertData(1000000, 10000, false));
        System.out.printf("%d\t%s\t%s\t%d\n", 1000000, "true", 100000, insertData(1000000, 100000, false));
        System.out.printf("%d\t%s\t%s\t%d\n", 1000000, "false", 500000, insertData(1000000, 500000, false));
    }

    public static long insertData(int number, int BatchSize, boolean AutoCommit) throws Exception {
        PreparedStatement ps = con.prepareStatement("INSERT INTO person.person (code) VALUES (?)");
        Random r = new Random();

        // AutoCommit
        con.setAutoCommit(AutoCommit);

        long startTime = System.currentTimeMillis();

        if(BatchSize==0){
            // Ohne batch processing
            for(int i=0; i<number; i++) {
                ps.setString(1, Long.toHexString(r.nextLong()));
                ps.executeUpdate();
            }
            ps.close();
        }else{
            // Mit batch processing
            for(int i=0; i<number; i++){
                ps.setString(1, Long.toHexString(r.nextLong()));
                ps.addBatch();

                if(i % BatchSize == 0){
                    ps.executeBatch();
                }
            }
            ps.executeBatch();
            ps.close();
        }

        long endTime = System.currentTimeMillis();
        long timeElapsed = ((endTime-startTime)/1000);

        return timeElapsed;
    }

    public static long Aufgabe2a(boolean AutoCommit) throws Exception {

		PreparedStatement ps = con.prepareStatement("SELECT id, code FROM person.person");
        con.setAutoCommit(AutoCommit);
		ResultSet rs = ps.executeQuery();
        long result = 0;

		while (rs.next()) {
            if(rs.getString("code").startsWith("a")){
                result++;
            }
		}
		rs.close();

        return result;	
    }

    public static long Aufgabe2b(boolean AutoCommit) throws Exception {
		PreparedStatement ps = con.prepareStatement("SELECT id, code FROM person.person");
        con.setAutoCommit(AutoCommit);
		ResultSet rs = ps.executeQuery();
        long result = 0;

        List<PersonRecord> persons = new ArrayList<>();

        while(rs.next()) {
            persons.add(new PersonRecord(rs.getInt("id"), rs.getString("code")));
        }
        rs.close();
        
        for(PersonRecord person: persons){
            if(person.code.startsWith("a")) {
                result++;
            }
        }
        return result;
    }

    public static long Aufgabe2c(boolean AutoCommit) throws Exception {
        PreparedStatement ps = con.prepareStatement("SELECT count(code) FROM person WHERE code LIKE 'a%'");
        con.setAutoCommit(AutoCommit);
        ResultSet rs = ps.executeQuery();
        
        rs.next();
        return rs.getLong("count");
    }

    public static long Aufgabe2d(boolean AutoCommit) throws Exception {
        PreparedStatement ps = con.prepareStatement("SELECT id, code FROM person");
        con.setAutoCommit(AutoCommit);
        ResultSet rs = ps.executeQuery();

        List<PersonRecord> persons = new ArrayList<>();

        while(rs.next()) {
            persons.add(new PersonRecord(rs.getInt("id"), rs.getString("code")));
        }
        rs.close();

        return persons
            .stream()
            .filter(p -> p.code.startsWith("a"))
            .count();
    }

    public static void Aufgabe2() throws Exception {
        long startTime;
        long startSize;
        long endTime;
        long endSize;
        long timeElapsed;
        long size;

        //insertData(5000000, 10, false);
        //con.commit();
        
        startTime = System.currentTimeMillis();
        startSize = Runtime.getRuntime().totalMemory();

        System.out.println(Aufgabe2a(false));
        System.out.println(Aufgabe2b(false));
        System.out.println(Aufgabe2c(false));
        System.out.println(Aufgabe2d(false));
        
        endTime = System.currentTimeMillis();
        endSize = Runtime.getRuntime().totalMemory();
        timeElapsed = ((endTime-startTime)/1000);
        size = endSize-startSize;

        System.out.println("Belegter Speicher: " + size);
        System.out.println("Dauer (s): " + timeElapsed);
    }
}
