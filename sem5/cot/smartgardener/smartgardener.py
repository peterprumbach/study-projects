from m5stack import *
from m5ui import *
from uiflow import *
import machine
import i2c_bus
import json
from machine import Pin
from machine import I2C
import ubinascii
from umqtt.simple import MQTTClient
from bmp280 import *
import dht12

setScreenColor(0x222222)

#MQTT-Einstellungen
mqtt_server = "localhost"
mqtt_port = 1883
mqtt_user = "board1"
mqtt_password = "board1"
mqtt_client_id = ubinascii.hexlify(machine.unique_id())
mqtt_topic = b"cot/board1/sensor"

#i2c Bus
bus = I2C(scl=Pin(22), sda=Pin(21), freq=30000)

#DHT12 0x5C
dht12 = dht12.DHT12(bus)
humidity = None
temperature = None

#BMP280 0x76
bmp = BMP280(bus)
bmp.use_case(BMP280_CASE_INDOOR)
air_pressure = None

#Helligkeitssensor
luminosity = Pin(5, Pin.IN, Pin.PULL_UP)

#Displayanzeige
light_text = M5TextBox(10, 51, "Licht:", lcd.FONT_DejaVu24, 0xFFFFFF, rotate=0) # Licht
light_value = M5TextBox(160, 51, "0", lcd.FONT_DejaVu40, 0xFFFFFF, rotate=0)

temp_text = M5TextBox(10, 91, "Temp:", lcd.FONT_DejaVu24, 0xFFFFFF, rotate=0) # Temperatur
temp_value = M5TextBox(160, 91, "0", lcd.FONT_DejaVu40, 0xFFFFFF, rotate=0)

luft_text = M5TextBox(10, 131, "Luft:", lcd.FONT_DejaVu24, 0xFFFFFF, rotate=0) # Luftfeuchtigkeit
luft_value = M5TextBox(160, 131, "0", lcd.FONT_DejaVu40, 0xFFFFFF, rotate=0)

air_text = M5TextBox(10, 171, "Luftdruck:", lcd.FONT_DejaVu24, 0xFFFFFF, rotate=0) # Luftdruck
air_value = M5TextBox(160, 171, "0", lcd.FONT_DejaVu40, 0xFFFFFF, rotate=0)

#Relay Wasserpumpe
relay = Pin(2, Pin.OUT)
relay.value(0)

#Servomotor
servo0 = unit.get(unit.SERVO, (13,13));
servo0.write_angle(90)

#Daten
data = {"id": ubinascii.hexlify(machine.unique_id()), "temperature": None, "humidity": None, "air_pressure": None,"light": None}

def callback(topic, msg):
  control = json.loads(msg)
  if control["function"] == "turn_on":
    relay.value(1)
    wait(control["duration"])
    relay.value(0)
  elif control["function"] == "turn_off":
    relay.value(0)
  elif control["function"] == "open_windows":
    servo0.write_angle(control["angle"])

client = MQTTClient(mqtt_client_id, mqtt_server, mqtt_port, mqtt_user, mqtt_password)
client.set_callback(callback)
client.connect()
client.subscribe(b"cot/board1/control")

while True:
  dht12.measure()
  temperature = dht12.temperature()
  humidity = dht12.humidity()
  wait_ms(100)
  bmp.normal_measure()
  air_pressure = bmp.pressure/100
  wait_ms(100)
  temp_value.setText(str(temperature))
  luft_value.setText(str(humidity))
  light_value.setText(str(luminosity.value()))
  air_value.setText(str(air_pressure))
  data["temperature"] = temperature
  data["humidity"] = humidity
  data["air_pressure"] = air_pressure
  data["light"] = luminosity.value()
  json_data = json.dumps(data)
  client.publish(mqtt_topic, b"{}".format(json_data), retain=False)
  wait_ms(100)
  client.check_msg()
  wait(5)