<?php

use App\Http\Controllers\DocumentController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProjectController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('lang/{locale}')->name("locale");  // unused, but needed for route definition in templates

Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/export/project/{project}')->name('export');  // unused, but needed for route definition in templates


    // Projects
    Route::get('/home/projects', [HomeController::class, 'listProjects'])
        ->name('home.projects.list');
    Route::get('/home/projects/join', [HomeController::class, 'listJoinProjects'])
        ->name('home.projects.join');
    Route::get('/home/projects/{project}/join', [ProjectController::class, 'attachUserToProject'])
        ->name('home.project.join');
    Route::get('/home/project/{project}/leave', [ProjectController::class, 'leaveProject'])
        ->name('home.project.leave');

    Route::get('/home/project/{project}', [ProjectController::class, 'details'])
        ->name('home.project.details');
    Route::get('/home/project/{project}/document/{document}/fill', [DocumentController::class, 'fillDocumentView'])
        ->name('home.document.fill');
    Route::post('/home/project/{project}/document/{document}/save', [DocumentController::class, 'saveDocument'])
        ->name('home.document.save');
});

Auth::routes();  // unused, but needed for route definition in templates
