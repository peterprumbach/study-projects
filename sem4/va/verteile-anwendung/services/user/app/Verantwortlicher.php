<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int document_id
 * @property int project_id
 * @property string verantwortlicher_name
 */
class Verantwortlicher extends Model
{
    public function document(){
        return $this->belongsTo(Document::class);
    }
    public function project(){
        return $this->belongsTo(Project::class);
    }

}
