<?php

namespace App\Http\Controllers;

use App\Models\Antwort;
use App\Models\CheckAntwort;
use App\Models\Document;
use App\Verantwortlicher;
use App\Models\Project;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class DocumentController extends Controller
{
    public function fillDocumentView(Project $project, Document $document)
    {
        $owner=Verantwortlicher::where([
            ['document_id', '=', $document->id],
            ['project_id', '=', $project->id]
        ])->first();


        if($owner){
            $owner=$owner->verantwortlicher_name;
        } else {
            $owner='';
        }

        return view('user.fillDocument', [
            'document' => $document,
            'project' => $project,
            'owner' =>$owner
        ]);
    }

    // TODO => Refactor the whole method
    // TODO => What is this method doing!? The name is editDocument, but at the end is something with Antwort
    public function saveDocument(Request $request, Project $project, Document $document)
    {
        $owner = $request->input('owner');

        $verantwortlicher = Verantwortlicher::where([
            ['document_id', '=', $document->id],
            ['project_id', '=', $project->id]
        ])->first();

        if(!$verantwortlicher){
            $verantwortlicher = new Verantwortlicher();
            $verantwortlicher->document_id = $document->id;
            $verantwortlicher->project_id = $project->id;

        }

        $verantwortlicher->document_id = $document->id;
        $verantwortlicher->project_id = $project->id;
        $verantwortlicher->verantwortlicher_name = $owner;
        $verantwortlicher->save();

        $antworten = $request->input('frage');

        foreach($antworten as $frageId => $antwort){
            $a = Antwort::where([
                ['frage_id', '=', $frageId],
                ['project_id', '=', $project->id]
            ])->first();

            if($a){
                $a->antwort = $antwort;
            } else {
                $a = new Antwort();
                $a->project_id = $project->id;
                $a->frage_id = $frageId;
                $a->antwort = $antwort;
            }
            $a->save();
        }

        $allCheckForDoc = $document->checklisten;
        $checkItemsPOST = $request->input('check');

        foreach($allCheckForDoc as $checkInDb){
            $checked = false;
            if(isset($checkItemsPOST[$checkInDb->id])){
                $checked = true;
            }
            $a = CheckAntwort::where([
                ['checkliste_item_id', '=', $checkInDb->id],
                ['project_id', '=', $project->id]
            ])->first();
            if($a){
                $a->checked = $checked;
            } else {
                $a = new CheckAntwort();
                $a->project_id = $project->id;
                $a->checkliste_item_id = $checkInDb->id;
                $a->checked = $checked;
            }
            $a->save();
        }


        return redirect()->back()->with('success', 'Antworten erfolgreich gespeichert.');
    }

    public function exportProject(Project $project){
        $documents = Document::all();
        $pdf = App::make('dompdf.wrapper');

        $pdf->loadView('export',[
            'project' => $project,
            'documents' => $documents
        ])->setPaper('a4','portrait');
        $fileName=$project->title;
        return $pdf->stream($fileName.'.pdf');
    }
}
