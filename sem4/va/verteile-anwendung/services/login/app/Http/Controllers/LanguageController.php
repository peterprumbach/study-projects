<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LanguageController extends Controller
{
    public function lang(Request $request, $locale)
    {
        App::setLocale($locale);
        session()->put('locale', $locale);
        if(App::getLocale() =='en'){
            $text='Language was change to english';
        }elseif (App::getLocale() =='de'){
            $text='Sprache wurde auf deutsch geändert';
        }
        return redirect()->back()->with('success', trans($text, [], $request->getLocale()));
    }
}
