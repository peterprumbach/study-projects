<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int project_id
 * @property int frage_id
 * @property string antwort
 */
class Antwort extends Model
{
    public function frage(){
        return $this->belongsTo(Frage::class);
    }
    public function project(){
        return $this->belongsTo(Project::class);
    }
}
