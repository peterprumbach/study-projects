<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Semester
 * @property array|string|null title
 * @package App\Models
 *
 */

class Semester extends Model
{
    public function projects(){
        return $this->hasMany(Project::class);
    }
}
