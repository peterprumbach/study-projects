@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __("My Projects") }}</div>
                    <div class="card-body">

                        <div class="list-group">
                            @if(count($myProjects)==0)
                                <a class="list-group-item list-group-item-action">{{ __("You have not yet joined a project.") }}</a>
                            @endif
                            @foreach($myProjects as $index => $p)
                                <a href="{{route('home.project.details', ['project' => $p])}}" class="list-group-item list-group-item-action">{{$p->title}}
                                    <span class="float-right">
                                        <button type="button" class="btn btn-outline-primary" title="{{ __("Leave Project") }}" data-toggle="modal" data-target="#modal_proj_{{$index}}">
                                            <i class="fas fa-sign-out-alt"></i>
                                        </button>
                                    </span>
                                </a>
                                <div class="modal fade" id="modal_proj_{{$index}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{ __("Leave Project?") }}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    {{ __("Are you sure you want to leave the project?") }}
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <a class="btn btn-secondary" href="#" data-dismiss="modal">{{ __("Cancel") }}</a>
                                                <a class="btn btn-primary" href="{{route('home.project.leave', ['project' => $p])}}">{{ __("Leave") }}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        <br>
                        <a class="btn btn-outline-primary" href="{{route('home.projects.join')}}">{{ __("Join Project") }}</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
