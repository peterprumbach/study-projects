@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __("Add Document") }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.document.add') }}">
                            @csrf
                            <div class="form-group">
                                <label for="title_de">{{ __("Title_de") }}</label>
                                <input type="type" class="form-control" id="title_de" name="title_de" required>
                                <small class="form-text text-muted">{{ __("Explanation Document Title DE") }}</small>
                            </div>
                            <div class="form-group">
                                <label for="beschreibung_de">{{ __("Description_de") }}</label>
                                <textarea class="form-control" id="beschreibung_de" rows="3" name="description_de"></textarea>
                                <small class="form-text text-muted">{{ __("Explanation Document Description DE") }}</small>
                            </div>
                            <div class="form-group">
                                <label for="title_en">{{ __("Title_en") }}</label>
                                <input type="type" class="form-control" id="title_en" name="title_en" required>
                                <small class="form-text text-muted">{{ __("Explanation Document Title EN") }}</small>
                            </div>
                            <div class="form-group">
                                <label for="beschreibung_en">{{ __("Description_en") }}</label>
                                <textarea class="form-control" id="beschreibung_en" rows="3" name="description_en"></textarea>
                                <small class="form-text text-muted">{{ __("Explanation Document Description EN") }}</small>
                            </div>


                            <br>
                            <small class="form-text text-muted">{{ __("Hint Questions can be added after creation") }}</small>
                            <br>
                            <button type="submit" class="btn btn-primary">{{ __("Create") }}</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
