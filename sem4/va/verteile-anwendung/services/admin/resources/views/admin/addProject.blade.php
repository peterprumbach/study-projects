@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Add Project') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.project.add') }}">
                            @csrf
                            <div class="form-group">
                                <label for="title">{{ __('Title') }}</label>
                                <input type="type" class="form-control" id="title" name="title" required>
                                <small class="form-text text-muted">{{ __("Explanation Project Title") }}</small>
                            </div>
                            <div class="form-group">
                                <label for="beschreibung">{{ __("Description") }}</label>
                                <textarea class="form-control" id="beschreibung" rows="3" name="description"></textarea>
                                <small class="form-text text-muted">{{ __("Explanation Description") }}</small>
                            </div>
                            <div class="form-group">
                                <label for="semester">{{ __("Semester") }}</label>
                                <select class="form-control" id="semester" name="semesterId">
                                    @foreach($semesters as $s)
                                        <option value="{{ $s->id }}">{{ $s->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">{{ __("Create") }}</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
