<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Project;
use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\App;

class ExportController extends Controller
{
    public function exportProject(Project $project){
        $documents = Document::all();
        $pdf = App::make('dompdf.wrapper');

        $pdf->loadView('export',[
            'project' => $project,
            'documents' => $documents
        ])->setPaper('a4','portrait');
        $fileName=$project->title;
        return $pdf->stream($fileName.'.pdf');
    }
}
