<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * @property int id
 * @property string text_de
 * @property string text_en
 * @property int document_id
 * @property int placement
 */
class ChecklisteItem extends Model
{
    public function get_text()
    {
        $lang = App::getLocale();
        switch ($lang) {
            case "en":
                //dd($this->text_en);
                return $this->text_en;
            case "de":
                //dd($this->text_en);
                return $this->text_de;
        }
        return "Meeeep";
    }
    public function document(){
        return $this->belongsTo(Document::class);
    }
    public function antwort(){
        return $this->hasMany(CheckAntwort::class);
    }

    public function getChecked($project){
        $a = $a = CheckAntwort::where([
            ['checkliste_item_id', '=', $this->id],
            ['project_id', '=', $project->id]
        ])->first();
        if($a){
            return $a->checked;
        } else{
            return false;
        }

    }
}
