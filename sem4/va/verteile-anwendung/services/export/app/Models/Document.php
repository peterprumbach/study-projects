<?php

namespace App\Models;

use App\Verantwortlicher;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * @property string title_de
 * @property string description_de
 * @property string title_en
 * @property string description_en
 * @property int id
 * @property Frage[] fragen
 * @property bool exclude_from_export
 * @property int placement
 */

class Document extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'placement'
    ];
    public function getDocumentsByOrder(){
        return Document::all()->sortBy('placement');

    }
    public function get_title(){
        $lang=App::getLocale();
        switch ($lang){
            case "en":

                return strval($this->title_en);
            case "de":

                return strval($this->title_de);
        }
        return "Meeeep";
    }
    public function get_description(){
        $lang=App::getLocale();
        switch ($lang){
            case "en":
                return $this->description_en;
            case "de":
                return $this->description_de;
        }
        return "Meeeep";
    }
    public function fragen(){
        return $this->hasMany(Frage::class);
    }

    public function checklisten(){
        return $this->hasMany(ChecklisteItem::class);
    }

    public function owner(){
        return $this->hasMany(Verantwortlicher::class);
    }

    public function getOwner(Project $project) {

        $owner = Verantwortlicher::where([
            ['document_id', '=', $this->id],
            ['project_id', '=', $project->id]
        ])->first();

        if($owner){
            return $owner->verantwortlicher_name;
        } else {
            return ;
        }

    }



    public function getProgress($project){
        $checkItems = ChecklisteItem::where([
            ['document_id', '=', $this->id],
        ])->get();
        $checkedCount = 0;
        foreach($checkItems as $c){
            if($c->getChecked($project)){
                $checkedCount++;
            }
        }
        $countAll = count($checkItems);
        if( $countAll > 0 && $countAll < 100 ){
            return ceil($checkedCount / count($checkItems) * 100);
        } else if($countAll > 0){
            return floor($checkedCount / count($checkItems) * 100);
        } else {
            return 0;
        }

    }
}
