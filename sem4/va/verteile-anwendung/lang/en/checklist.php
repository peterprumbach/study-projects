<?php

return [
    "item" => [
        "add" => [
            "success" => "Checklist item \":title\" successfully created.",
            "error" => "An error occurred."
        ],
        "edit" => [
            "success" => "Checklist item \":title\" successfully edited.",
            "error" => "An error occurred."
        ],
        "delete" => [
            "success" => "Checklist item successfully deleted.",
            "error" => "An error occurred."
        ]
    ]
];
