<?php

return [
    "create" => [
        "success" => "Semester \":title\" successfully created.",
        "error" => "An error occurred."
    ],
    "delete" => [
        "success" => "Semester \":title\" successfully deleted!",
        "error" => "An error occurred."
    ]
];
