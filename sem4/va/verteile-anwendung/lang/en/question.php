<?php

return [
    "add" => [
        "success" => "Question \":title\" successfully created.",
        "error" => "An error occurred."
    ],
    "edit" => [
        "success" => "Question \":title\" successfully edited.",
        "error" => "An error occurred."
    ],
    "level" => [
        "error" => "An error occurred."
    ],
    "delete" => [
        "success" => "Question successfully deleted.",
        "error" => "An error occurred."
    ]
];
