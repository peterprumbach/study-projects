<?php

return [
    "create" => [
        "success" => "Projekt \":title\" erfolgreich angelegt.",
        "error" => "Es ist ein Fehler aufgetreten"
    ],
    "delete" => [
        "success" => "Projekt \":title\" erfolgreich gelöscht!",
        "error" => "Es ist ein Fehler aufgetreten"
    ],
    "save" => [
        "success" => "Projekt \":title\" erfolgreich gespeichert.",
        "error" => "Es ist ein Fehler aufgetreten"
    ],
    "join" => [
        "warning" => "Sie sind bereits in diesem Projekt",
        "success" => "Projekt \":title\" erfolgreich beigetreten."
    ],
    "leave" => [
        "success" => "Projekt \":title\" erfolgreich verlassen."
    ]
];
