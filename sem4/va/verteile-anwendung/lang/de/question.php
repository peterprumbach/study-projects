<?php

return [
    "add" => [
        "success" => "Frage \":title\" erfolgreich angelegt.",
        "error" => "Ein Fehler ist aufgetreten"
    ],
    "edit" => [
        "success" => "Frage \":title\" erfolgreich angelegt.",
        "error" => "Ein Fehler ist aufgetreten"
    ],
    "level" => [
        "error" => "Es ist ein Fehler aufgetreten."
    ],
    "delete" => [
        "success" => "Frage erfolgreich gelöscht.",
        "error" => "Ein Fehler ist aufgetreten."
    ]
];
