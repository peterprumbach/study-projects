<?php

return [
    "create" => [
        "success" => "Dokument \":title\" erfolgreich angelegt!",
        "error" => "Ein Fehler ist aufgetreten."
    ],
    "save" => [
        "success" => "Dokument \":title\" erfolgreich gespeichert."
    ],
    "delete" => [
        "error" => "Ein Fehler ist aufgetreten."
    ]
];
